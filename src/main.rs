#![windows_subsystem = "windows"]

use nannou::prelude::*;
use nannou_egui::{egui, Egui};

fn main() {
    nannou::app(model).update(update).view(view).run();
}

type NodeID = usize;

struct Model {
    nodes: Vec<Node>,
    connections: Vec<Connection>,
    dragging: Option<(NodeID, Vec2)>,

    egui: Egui,
    settings: Settings,
}

struct Settings {
    node_radius: f32,
    num_nodes: usize,
    spring_d: f32,
    node_mass: f32,
    gravity: f32,
    friction_factor: f32,
    fixed_end: bool,
    horizontal: bool,
}

impl Default for Settings {
    fn default() -> Self {
        Self {
            node_radius: 15.,
            num_nodes: 10,
            spring_d: 2.,
            node_mass: 1.,
            gravity: 9.81,
            friction_factor: 0.98,
            fixed_end: false,
            horizontal: false,
        }
    }
}

struct Node {
    pos: Vec2,
    vel: Vec2,
    acc: Vec2,
}

impl Node {
    fn new(pos: Vec2) -> Self {
        Self {
            pos,
            vel: Vec2::ZERO,
            acc: Vec2::ZERO,
        }
    }

    fn contains(&self, pos: Vec2, node_radius: f32) -> bool {
        (self.pos - pos).length_squared() <= node_radius * node_radius
    }

    fn clamp(&mut self, rect: Rect, node_radius: f32) {
        self.pos = self.pos.clamp(
            vec2(rect.left() + node_radius, rect.bottom() + node_radius),
            vec2(rect.right() - node_radius, rect.top() - node_radius),
        );
    }
}

struct Connection {
    start: NodeID,
    end: NodeID,
}

fn model(app: &App) -> Model {
    app.set_exit_on_escape(false);

    app.new_window()
        .title("Wellen-Simulation")
        .size(400, 800)
        .min_size(200, 300)
        .resized(resized)
        .resizable(true)
        .raw_event(raw_window_event)
        .mouse_pressed(mouse_pressed)
        .mouse_released(mouse_released)
        .mouse_moved(mouse_moved)
        .build()
        .unwrap();

    let settings = Settings::default();
    let (nodes, connections) = setup_nodes_and_connections(app, &settings);
    Model {
        nodes,
        connections,
        dragging: None,
        egui: Egui::from_window(&app.main_window()),
        settings,
    }
}

fn setup_nodes_and_connections(app: &App, settings: &Settings) -> (Vec<Node>, Vec<Connection>) {
    let win = app.window_rect();

    let mut nodes = Vec::with_capacity(settings.num_nodes);
    let mut connections = Vec::with_capacity(settings.num_nodes - 1);

    if settings.horizontal {
        let node_dist = (win.w() - 100.) / (settings.num_nodes - 1) as f32;
        let start = win.left() + 50.;
        nodes.push(Node::new(vec2(start, 0.)));
        for i in 1..settings.num_nodes {
            let x = start + node_dist * i as f32;
            nodes.push(Node::new(vec2(x, 0.)));
            connections.push(Connection {
                start: i - 1,
                end: i,
            });
        }
    } else {
        let mut y = win.top() - 50.;

        // let mut y = win.top() - 50.;
        nodes.push(Node::new(vec2(0., y)));
        for i in 1..settings.num_nodes {
            // F / 2 = Ds / 2 = mg <=> s = 2 * mg/D
            let prev_s =
                2. * settings.gravity * settings.node_mass * (settings.num_nodes - i) as f32
                    / settings.spring_d;
            y -= prev_s;
            nodes.push(Node::new(vec2(0., y)));
            connections.push(Connection {
                start: i - 1,
                end: i,
            });
        }
    }

    (nodes, connections)
}

fn resized(app: &App, model: &mut Model, _: Vec2) {
    (model.nodes, model.connections) = setup_nodes_and_connections(app, &model.settings);
}

fn raw_window_event(_app: &App, model: &mut Model, event: &nannou::winit::event::WindowEvent) {
    model.egui.handle_raw_event(event);
}

fn mouse_pressed(app: &App, model: &mut Model, button: MouseButton) {
    let mouse_pos = vec2(app.mouse.x, app.mouse.y);
    if button == MouseButton::Left {
        for (i, node) in model.nodes.iter().enumerate() {
            if node.contains(mouse_pos, model.settings.node_radius) {
                model.dragging = Some((i, node.pos - mouse_pos));
                break;
            }
        }
    }
}

fn mouse_released(_: &App, model: &mut Model, button: MouseButton) {
    if button == MouseButton::Left {
        if let Some((i, _)) = model.dragging {
            let node = &mut model.nodes[i];
            node.vel = Vec2::ZERO;
            node.acc = Vec2::ZERO;
            model.dragging = None;
        }
    }
}

fn mouse_moved(app: &App, model: &mut Model, mouse_pos: Point2) {
    if let Some((i, offset)) = model.dragging {
        let node = &mut model.nodes[i];
        node.pos = mouse_pos + offset;
        node.clamp(app.window_rect(), model.settings.node_radius);
    }
}

fn update(app: &App, model: &mut Model, update: Update) {
    // ui
    model.egui.set_elapsed_time(update.since_start);
    let ctx = model.egui.begin_frame();

    egui::Window::new("Einstellungen").show(&ctx, |ui| {
        let mut setup = ui
            .add(egui::Slider::new(&mut model.settings.num_nodes, 2..=30).text("Anzahl"))
            .changed();
        ui.add(egui::Slider::new(&mut model.settings.node_radius, 5.0..=30.).text("Radius"));
        ui.add(egui::Slider::new(&mut model.settings.node_mass, 0.1..=100.).text("Masse"));
        ui.add(
            egui::Slider::new(&mut model.settings.gravity, 0.0..=100.).text("Erdbeschleunigung"),
        );
        ui.add(egui::Slider::new(&mut model.settings.spring_d, 0.0..=50.).text("Federhärte"));
        ui.add(
            egui::Slider::new(&mut model.settings.friction_factor, 0.0..=1.)
                .text("Reibungskoeffizient"),
        );
        ui.horizontal(|ui| {
            setup |= ui
                .add(egui::Checkbox::new(
                    &mut model.settings.horizontal,
                    "Horizontal",
                ))
                .changed();
            ui.add(egui::Checkbox::new(
                &mut model.settings.fixed_end,
                "Fixiertes Ende",
            ));
        });
        setup |= ui.button("Reset").clicked();
        if setup {
            (model.nodes, model.connections) = setup_nodes_and_connections(app, &model.settings);
        }
    });

    // simulate the connections as springs
    for connection in &model.connections {
        let s = model.nodes[connection.start]
            .pos
            .distance(model.nodes[connection.end].pos);
        let f = s * model.settings.spring_d / 2.;

        let start = &model.nodes[connection.start];
        let end = &model.nodes[connection.end];

        let f_start = (end.pos - start.pos).normalize_or_zero() * f;
        let f_end = -f_start;

        model.nodes[connection.start].acc += f_start / model.settings.node_mass;
        model.nodes[connection.end].acc += f_end / model.settings.node_mass;
    }

    // gravity
    for node in model.nodes.iter_mut().skip(1) {
        node.acc.y -= model.settings.gravity;
    }

    // euler integration
    let len = model.nodes.len();
    if len == 0 {
        return;
    }

    if model.settings.fixed_end {
        let end = &mut model.nodes[len - 1];
        end.acc = Vec2::ZERO;
        end.vel = Vec2::ZERO;
    }

    for (i, node) in model.nodes.iter_mut().enumerate().skip(1) {
        if let Some((dragged, _)) = model.dragging {
            if i == dragged {
                continue;
            }
        }

        node.vel += node.acc;
        node.vel *= model.settings.friction_factor;
        node.vel = node.vel.clamp_length_max(10000.);

        node.pos += node.vel * update.since_last.secs() as f32;

        // reset the resulting force
        node.acc = Vec2::ZERO;
    }
}

fn view(app: &App, model: &Model, frame: Frame) {
    frame.clear(gray(40u8));

    let draw = app.draw();

    for connection in &model.connections {
        let start = &model.nodes[connection.start];
        let end = &model.nodes[connection.end];

        draw.line()
            .start(start.pos)
            .end(end.pos)
            .caps_round()
            .color(rgba8(235, 219, 178, 50))
            .stroke_weight(2.);
    }

    let len = model.nodes.len();
    for (i, node) in model.nodes.iter().enumerate() {
        let color = if i == 0 || i == len - 1 && model.settings.fixed_end {
            rgba8(69, 133, 136, 100)
        } else {
            rgba8(146, 131, 116, 100)
        };
        draw.ellipse()
            .xy(node.pos)
            .radius(model.settings.node_radius)
            .color(color);
    }

    draw.to_frame(app, &frame).unwrap();

    model.egui.draw_to_frame(&frame).unwrap();
}
